#! /bin/bash
echo "Работа с инструментарием гит"
index()
{ 
   git status
   echo "Введите имя файла для индексирования"
   read filename
   git add $filename
}
add()
{
echo "Выберите действие:
		создать репо - 1 
		создать ветку - 2
		создать коммит - 3"
read action
case "$action" in
	1)
	echo "Выберите путь для создания репо"
	read path
	git init $path
	echo "Привязать локальный репо к удаленному[y/n]?"
         read ans 
         case $ans in
            y)
            echo "Введите ссылку на удаленный репозиторий"
            read gitrem
            git remote add origin $gitrem 
            ;;
            n)
            ;;  	
            *)
			;;		
         esac
		;;
	2) #создать ветку
         echo "Введите полный путь к нужному репозиторию" 
         read repname
         cd
         cd  $repname
         echo "Список всех веток репозитория"
         git branch
         echo "Введите имя новой ветки" 
         read branchname
         git branch $branchname
         echo "Список всех веток репозитория"
         git branch
		;;
	3) # создаем коммит
         echo "Введите полный путь к нужному репозиторию" 
         read repname
         cd
         cd  $repname
         git status
         echo "Хотите выполнить индексирование файлов[y/n]?"
         read ansind
         case $ansind in
            y)
             index          
             echo "Введите комментарий к коммиту" 
             read comment
             git commit -m "$comment"
             ;;
            n)            
             echo "Введите комментарий к коммиту" 
             read comment
             git commit -m "$comment"
             ;;
            *)
			;;
          esac   
        ;;
esac
}
edit()
 {
  echo "Введите полный путь к репозиторию "
      read repname
      cd
      cd  $repname
  echo "Редактирование
        Изменить коммит      -1
        Переименовать ветку  -2
        Смержить ветки       -3
        "
  read editvar
  case $editvar in
    1)
      echo "Хотите выполнить индексирование  файлов[y/n]?"
      read ansind
      case $ansind in
          y)
            index          
            git commit --amend
            ;;
          n)            
            git commit --amend
            ;;
      esac    
      git status     
      ;;  
    2)
      echo "Введите имя ветки для переименования"
      read oldname
      git checkout $oldname
      echo "Введите новое имя ветки"
      read newname
      git branch -b $newname
      git branch
      ;;
    3)
      echo "Введите имя текущей ветки"
      read current
      git checkout $current
      echo "Введите имя ветки для слияния с текущей"
      read second
      git merge $second
      git branch
      ;;
  esac 
 }
del()
{
	echo "Удалить
       1- репозиторий 
       2- ветку
       3- commit"
  read var_del
  case $var_del in 
      1) #удаляем репозиторий
         echo "Введите полный путь к репозиторию для удаления"
         read repname
         cd
         rm -rf $repname
         ;;
      2) #удаляем ветку
         echo "Введите имя репозитория"
         read repname
         cd
         cd  $repname
         echo "Список всех веток репозитория"
         git branch
         echo "Введите имя ветки для удаления" 
         read branchname
         git  branch -D $branchname
         echo "Список всех веток репозитория"
         git branch
         ;;
      3) # откат на предыдущий коммит
         echo "Введите имя репозитория"
         read repname
         cd
         cd  $repname
         echo "Выберите коммит для отката"
         git log
         echo "Введите хэш коммита для отката"
         read hesh
         git revert $hesh
         ;; 
  esac
}
  

pullclone()
 {
  echo "Забрать изменения(p) или клонировать репозиторий(c)?"
  read varpc
  case $varpc in
    p)
     git branch
     echo "Выберите ветку в которую забрать изменения"
     read branchname
     git checkout $branchname
     git branch
     git pull
    ;;
    c)
      echo "Введите полный путь для папки клонирования"
      read repname
      cd
      cd  $repname
      echo "Введите ссылку на репо ,изпользуя протокол http"
      read linkrepo
      git clone $linkrepo
    ;;
  esac  
  }  
pushrep()
 {
    echo "Введите полный путь к репозиторию"
    read repname
    cd
    cd  $repname
    git branch
    echo "Выберите ветку которую запушить"
    read branchname
    git checkout $branchname
    git branch
    echo "Введите имя ветки в удаленном репо"
    read branchname2
    git push origin $branchname2
 } 
  branchswitch()
 {
    echo "Введите полный путь к репозиторию"
    read repname
    cd
    cd  $repname
    git branch
    echo "Введите имя ветки"
    read branchname
    git checkout $branchname

 }
 echo "Выберете операцию для работы с git:
      Создать(репо,ветку,коммит)                  -a
      Редактирование(коммита,ветку)               -e
      Удалить(репо,ветку,коммит)                  -d 
      Переключение веток                          -s  
      Индексация изменений                        -i
      Отправка в удаленный репо                   -p
      Получение из удаленного репо                -k
      Выход                                       -q
      "
read action
case "$action" in # взависимости от передаваемого параметра выбираем действие
	a) # создание
  	add
  	cd
  	cd /home/hromlolpro/agafon/eltex/Scripts
  	./laba3.sh
  	;;
	e) #редактирвоание
  edit
  cd
  cd /home/hromlolpro/agafon/eltex/Scripts
  ./laba3.sh
  ;;
	d) # удаление
  del
  cd
  cd /home/hromlolpro/agafon/eltex/Scripts
  ./laba3.sh
  ;; 
s)
  branchswitch
  cd
  cd /home/hromlolpro/agafon/eltex/Scripts
  ./laba3.sh
  ;;  
i) # индексирование
  echo "Введите полный путь к репозиторию "
  read repname
  cd
  cd  $repname
  index
  cd
  cd /home/hromlolpro/agafon/eltex/Scripts
  ./laba3.sh
  ;;
p) # запушить в репо
 pushrep
  cd
  cd /home/hromlolpro/agafon/eltex/Scripts
  ./laba3.sh
 ;;
k) # забрать из удаленного репо
  pullclone
  cd
  cd /home/hromlolpro/agafon/eltex/Scripts
  ./laba3.sh
  ;;
q)
  exit
  ;;
esac