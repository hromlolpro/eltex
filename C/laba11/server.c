#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <errno.h>
#include <netinet/in.h>
#define PORT 2000
#define drones 10

 struct data{
	int* targets;
	int** map;
	int sizemap;
	int n;
	int sockets;
};

typedef struct data data;
static pthread_mutex_t shared_mutex  = PTHREAD_MUTEX_INITIALIZER;

int** mapcreate (int sizemap)
{	
	srand(getpid());
	int **map;
	map = malloc(sizemap * sizeof(int*));
	for (int i = 0; i < sizemap; ++i)
	{
		map[i] = malloc(sizemap * sizeof(int));
		for (int j = 0; j < sizemap; ++j)
		{
			map[i][j] = rand()%2;
			printf("%d  ", map[i][j]);
		}
		printf("\n");
	}
	return map;
}


void* thread (void* arg)
{
	
	data* data_drones = (data*) arg;
	int *targets = data_drones->targets;
	int sizemap = data_drones->sizemap;
	int **map = data_drones->map;
	int n;
	int sockets;
	sockets = data_drones->sockets;

	pthread_mutex_lock(&shared_mutex);
	n = data_drones->n;
	data_drones->n++;
	pthread_mutex_unlock(&shared_mutex);

	int conn = accept(sockets, NULL, NULL);
	int koord[2];
	int var;
	send (conn, &sizemap, sizeof(int), 0);
	recv (conn, koord, sizeof(koord), 0);
	recv (conn, &var, sizeof(int), 0);
	int a = koord[0];
	int b = koord[1];
	srand(pthread_self());

		switch (var){
			case 0:
				switch (rand()%2){
					case 0: //Проходка от а до sizemap
						for (int i = b; i < sizemap; ++i)
						{
							if (map[a][i] == 1)
								targets[n]++;
						}
						
						break;
					case 1: //Проходка от b до sizemap
						for (int i = a; i < sizemap; ++i)
						{
							if (map[i][b] == 1)
								targets[n]++;
						}
						
						break;
				}
				break;
			case 1:
				switch (rand()%2){
					case 0: 
						for (int i = b; i >= 0; i--)
						{
							if (map[a][i] == 1)
								targets[n]++;
						}
						
						break;
					case 1:
						for (int i = a; i < sizemap; ++i)
						{
							if (map[i][b] == 1)
								targets[n]++;
						}
						

						break;
				}
				break;
			case 2:
				switch (rand()%2){
					case 0: 
						for (int i = b; i < sizemap; ++i)
						{
							if (map[a][i] == 1)
								targets[n]++;
						}
						
						break;
					case 1:
						for (int i = a; i >= 0; i--)
						{
							if (map[i][b] == 1)
								targets[n]++;
						}
						
						break;
				}
				break;
			case 3:
				switch (rand()%2){
					case 0: 
						for (int i = b; i >= 0; i--)
						{
							if (map[a][i] == 1)
								targets[n]++;
						}
						
						break;
					case 1:
						for (int i = a; i >= 0; i--)
						{
							if (map[i][b] == 1)
								targets[n]++;
						}
						
						break;
				}
				break;			
		}
	send(conn, &targets[n], sizeof(targets[n]), 0);
	close(conn);
	

}

int main(int argc, char const *argv[])
{
	data data_main;
	int** map;
	int sizemap = atoi(argv[1]);
	data_main.sizemap=sizemap;
	data_main.targets=malloc(sizeof(int)*drones);
	map = mapcreate (sizemap);
	data_main.map=map;

	int sockets;
	struct sockaddr_in serv_addr;
	sockets = socket (AF_INET, SOCK_STREAM, 0);
	data_main.sockets=sockets;
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	serv_addr.sin_port = htons(PORT);
	int yes=1;

	if (setsockopt(sockets,SOL_SOCKET,SO_REUSEADDR|SO_REUSEPORT,&yes,sizeof(int)) == -1)
	{
	  perror("setsockopt");
	  exit(1);
	}


	bind (sockets, (struct sockaddr*) &serv_addr, sizeof(serv_addr));
	printf ("Server start working\n");
	listen (sockets, SOMAXCONN);
	pthread_t tid[drones];
	/*while(1)
	{
	 	pthread_create(&tid, NULL, thread, &data_main);
	 	//pthread_join(tid, 0);
	}
*/
	for (int i = 0; i < drones; ++i)
	{
		pthread_create(&tid[i], NULL, thread, &data_main);
	}
	for (int i = 0; i < drones; ++i)
	{
		pthread_join(tid[i], 0);
	}



	return 0;
}