#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#define drones 5
#define PORT 2000


int solution (int a, int b, int sizemap)
{
	int rest_a = sizemap - a; //Расстояние от правого края
	int rest_b = sizemap - b; //Расстояние от нижнего края
	int var;
	if (rest_a >= a && rest_b > b)
	{
	 	var = 0;
	}
	else if (rest_a < a && rest_b >= b)
	{
		var = 2;
	}
	else if (rest_a > a && rest_b <= b)
	{
		var = 1;
	}
	else if (rest_a <= a && rest_b < b)
	{
		var = 3;
	}
	else
	{
		var = rand()%4;
	}
	return(var);
}

void* thread(void* arg)
{
	int sockets;
	struct sockaddr_in serv_addr;
	sockets = socket (AF_INET, SOCK_STREAM, 0);
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
	serv_addr.sin_port = htons(PORT);
	 if(connect(sockets, (struct sockaddr *)&serv_addr, sizeof(serv_addr))<0)
          {   //если соединение не установлено
            printf(" Error : Connect Failed \n");
        }
	int sizemap;
	recv (sockets, &sizemap, sizeof(int), 0);
		srand(pthread_self());
		int a = rand()%sizemap; //Начальная точка дрона в строке
		int b = rand()%sizemap; //Начальная точка дрона в столбце
		printf("Разведчик №[%lu] упал в a=%d, b=%d\n", pthread_self(), a, b);
		int var = solution(a, b, sizemap);
		int koord[2];
		koord[0] = a;
		koord[1] = b;
		int targets;
	send(sockets, koord, sizeof(koord), 0);
	send(sockets, &var, sizeof(int), 0);
	recv(sockets, &targets, sizeof(int), 0);
	printf("Кол-во целей = %d\n", targets);



}



int main(int argc, char const *argv[])
{
	pthread_t tid[drones];
	for (int i = 0; i < drones; ++i)
	{
		pthread_create(&tid[i], 0, thread, NULL);
	}
	 for (int i = 0; i < drones; i++)
    {
       pthread_join(tid[i],NULL);
    }
	
	return 0;
}