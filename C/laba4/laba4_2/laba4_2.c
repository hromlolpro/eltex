#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

int main(int argc, char const *argv[])
{
	FILE *fp1, *fp2;
	char c;
	int count=0;
	fp1=fopen(argv[1], "r");
	fp2=fopen("output", "w");
		while((c=getc(fp1)) != EOF)
		{
			if ((/*'0' <= c && c <= '9'*/ isdigit(c)) && (count <= atoi(argv[2])))
			{
				fputc(' ', fp2);
				count++;
			}
			else
			{
				fputc(c, fp2);
			}
		}
	fclose(fp1);
	fclose(fp2);
	return 0;
}