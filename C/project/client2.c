#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <errno.h>
#include <netinet/in.h>
#include <sys/msg.h>
#include <sys/wait.h>
#include <string.h>
#include <arpa/inet.h>
#define MAXSIZE 500
#define UDP_PORT 3000

struct udp_struct
{
	char message[MAXSIZE];
	int tcp_port;
};

typedef struct udp_struct udp_struct;

struct tcp_msg // сообщение по TCP
{
	long mtype;
	int T;
	int strlen;
	char str[MAXSIZE];
};

typedef struct tcp_msg tcp_msg;

int main(int argc, char const *argv[])
{
	udp_struct recvdmsg_udp;
	int UDP_socket, bytes_read = 0;
	
	//создание UDP сокета
	UDP_socket = socket(AF_INET, SOCK_DGRAM, 0);
	struct sockaddr_in UDP_addr;
	UDP_addr.sin_family = AF_INET;
	UDP_addr.sin_port = htons(UDP_PORT);
	UDP_addr.sin_addr.s_addr = htonl(INADDR_ANY);

	if (bind(UDP_socket, (struct sockaddr *)&UDP_addr, sizeof(UDP_addr)) < 0)
	{
		perror("bind");
		sleep(3);
		return(-1);
	}

		printf("Ждёт коннекта\n");
	while(1)
		{
			if ((bytes_read = recvfrom(UDP_socket, &recvdmsg_udp, sizeof(recvdmsg_udp), 0, NULL, NULL)) > 0)
			{
				if (recvdmsg_udp.message[0] == 'M')
				{
					
					printf("%s , bytes_read = %d\n Для вас выделен порт = %d\n", recvdmsg_udp.message, bytes_read, recvdmsg_udp.tcp_port);
					break;
				}
			}
			sleep(1);
		}

	int yes=1;	
	if (setsockopt(UDP_socket,SOL_SOCKET,SO_REUSEADDR|SO_REUSEPORT,&yes,sizeof(int)) == -1)
	{
	  perror("setsockopt");
	  exit(1);
	}
	close(UDP_socket);

	int TCP_PORT2 = recvdmsg_udp.tcp_port;

	//создание TCP сокета
	int tcp_socket = socket (AF_INET, SOCK_STREAM, 0);

	struct sockaddr_in TCP_addr;
	TCP_addr.sin_family = AF_INET;
	TCP_addr.sin_port = htons(TCP_PORT2);
	TCP_addr.sin_addr.s_addr = inet_addr("127.0.0.1");

	tcp_msg recvdmsg_tcp;

	memset(recvdmsg_tcp.str, 0, MAXSIZE);



	if(connect(tcp_socket, (struct sockaddr *)&TCP_addr, sizeof(TCP_addr))<0)
		printf(" Error : Connect Failed \n");
	
	printf("Пошло соединение по TCP!\n");
	
	while(1)
	{
		
		recv(tcp_socket, &recvdmsg_tcp, sizeof(recvdmsg_tcp), 0);
		printf("\033[34mВремя\033[0m = %d\n\033[34mДлина\033[0m = %d\n\033[34mСтрока\033[0m =  %s\n", recvdmsg_tcp.T, recvdmsg_tcp.strlen, recvdmsg_tcp.str);
		sleep(recvdmsg_tcp.T);
	}

return 0;
}
