#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <errno.h>
#include <netinet/in.h>
#include <sys/msg.h>
#include <sys/wait.h>
#include <arpa/inet.h>
#include <time.h>
#include <string.h>
#define SLEEPTIME 5
#define MAXSIZE 500
#define UDP_PORT 4000 // UDP-порт

struct udp_struct
{
	char message[MAXSIZE];
	int tcp_port;
};

typedef struct udp_struct udp_struct;

struct tcp_msg // сообщение по TCP
{
	long mtype;
	int T;
	int strlen;
	char str[MAXSIZE];
};

typedef struct tcp_msg tcp_msg;

int main(int argc, char const *argv[])
{
	udp_struct recvdmsg_udp;
	int UDP_socket, bytes_read;

	//создание UDP сокета
	UDP_socket = socket(AF_INET, SOCK_DGRAM, 0);
	struct sockaddr_in UDP_addr;
	UDP_addr.sin_family = AF_INET;
	UDP_addr.sin_port = htons(UDP_PORT);
	UDP_addr.sin_addr.s_addr = htonl(INADDR_ANY);


	bind(UDP_socket, (struct sockaddr *)&UDP_addr, sizeof(UDP_addr));

	while(1)
		{
			if ((bytes_read = recvfrom(UDP_socket, &recvdmsg_udp, sizeof(recvdmsg_udp), 0, NULL, NULL)) > 0)
			{	
				if (recvdmsg_udp.message[0] == 'W')
				{
					printf("%s , bytes_read = %d\n Для вас выделен порт = %d\n", recvdmsg_udp.message, bytes_read, recvdmsg_udp.tcp_port);
					break;
				}	
			}
			sleep(1);

		}
	
	int yes=1;
	if (setsockopt(UDP_socket,SOL_SOCKET,SO_REUSEADDR|SO_REUSEPORT,&yes,sizeof(int)) == -1)
	{
	  perror("setsockopt");
	  exit(1);
	}
	close(UDP_socket);

	int TCP_PORT1 = recvdmsg_udp.tcp_port;

	//создание TCP сокета
	int tcp_socket = socket (AF_INET, SOCK_STREAM, 0);

	struct sockaddr_in TCP_addr;
	TCP_addr.sin_family = AF_INET;
	TCP_addr.sin_port = htons(TCP_PORT1);
	TCP_addr.sin_addr.s_addr = inet_addr("127.0.0.1");

	tcp_msg sendmsg_tcp;

	memset(sendmsg_tcp.str, 0, MAXSIZE);
	if(connect(tcp_socket, (struct sockaddr *)&TCP_addr, sizeof(TCP_addr))<0)
		printf(" Error : Connect Failed \n");
	
	printf("Пошло соединение по TCP!\n");
	
	while(1)
	{
		srand(time(NULL));
		sendmsg_tcp.T = (rand()%SLEEPTIME)+1;
		sendmsg_tcp.strlen = rand()%MAXSIZE;
		for (int i = 0; i < sendmsg_tcp.strlen-1; ++i)
		{
			sendmsg_tcp.str[i] = rand()%(123-97)+97;
		}
		//sendmsg_tcp.str[sendmsg_tcp.strlen] = '\0';
		printf("\033[32mВремя\033[0m = %d\n\033[32mДлина\033[0m = %d\n\033[32mСтрока\033[0m =  %s\n", sendmsg_tcp.T, sendmsg_tcp.strlen, sendmsg_tcp.str);
		send(tcp_socket, &sendmsg_tcp, sizeof(sendmsg_tcp), 0);
		sleep(sendmsg_tcp.T);
	}

return 0;
}

