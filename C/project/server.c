#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <errno.h>
#include <netinet/in.h>
#include <sys/msg.h>
#include <sys/wait.h>
#include <string.h>
#define MAXSIZE 500
#define UDP_PORT1 4000
#define UDP_PORT2 3000
#define TCP_PORT1 5000 // Порт для клиента 1 типа
#define TCP_PORT2 6000 // Порт для клиента 2 типа

#define N 10

struct tcp_msg // сообщение по TCP
{
	long mtype;
	int T;
	int strlen;
	char str[MAXSIZE];
};
typedef struct tcp_msg tcp_msg;

struct udp_struct
{
	char message[MAXSIZE];
	int tcp_port;
};

typedef struct udp_struct udp_struct;

struct client_struct
{
	int type;
	int msqid;
	int connSock;
};

typedef struct client_struct client_struct;

static pthread_mutex_t shared_mutex  = PTHREAD_MUTEX_INITIALIZER;

void* thread_UDP(void* arg_UPD)
{
	pthread_detach(pthread_self());
	udp_struct udp_msg;
	int msqid =  *(int*)arg_UPD;
	char msg_send[] = "Waiting for message";
	char msg_recieve[] = "Message exist";

	int UDP_socket1, UDP_socket2;
	struct msqid_ds qstatus;
	//создание UDP сокета
	UDP_socket1 = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

	struct sockaddr_in UDP_addr1;
	UDP_addr1.sin_family = AF_INET;
	UDP_addr1.sin_port = htons(UDP_PORT1);
	UDP_addr1.sin_addr.s_addr = htonl(INADDR_LOOPBACK);

	UDP_socket2 = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

	struct sockaddr_in UDP_addr2;
	UDP_addr2.sin_family = AF_INET;
	UDP_addr2.sin_port = htons(UDP_PORT2);
	UDP_addr2.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
	
	while(1)
	{
		msgctl (msqid, IPC_STAT, &qstatus);
		printf("\033[33mКол-во сообщений в очереди\033[0m = \033[31m%ld\n\033[0m", qstatus.msg_qnum);
		if (qstatus.msg_qnum < N)
		{	
			strcpy(udp_msg.message, msg_send);
			udp_msg.tcp_port = TCP_PORT1;
			int temp=sendto(UDP_socket1, &udp_msg, sizeof(udp_msg), 0, (struct sockaddr *)&UDP_addr1, sizeof(UDP_addr1));
			printf("\033[31mОтправлено кол-во байт =\033[0m %d\n", temp);
		}

		if (qstatus.msg_qnum > 0)
		{
			strcpy(udp_msg.message, msg_recieve);
			udp_msg.tcp_port = TCP_PORT2;
			sendto(UDP_socket2, &udp_msg, sizeof(udp_msg), 0, (struct sockaddr *)&UDP_addr2, sizeof(UDP_addr2));
		}
		sleep(2);
	}
}

void* client_thread(void* arg_client)
{
	struct msqid_ds qstatus;
	pthread_detach(pthread_self());
	client_struct client_data = *(client_struct*)arg_client;
	tcp_msg buf;
	while(1)
	{	
		switch (client_data.type)
		{
			case 1:
				if ((recv(client_data.connSock, &buf, sizeof(buf), 0)) <= 0)
				{
					perror("\033[31mSERVER\033[0m: recv() failed");
					sleep(5);
					pthread_exit(0);
				}
				printf("Принято TCP-сообщение\n \033[31m Время \033[0m = %d\n \033[31m Длина \033[0m = %d\n \033[31m Строка \033[0m =  %s\n", buf.T, buf.strlen, buf.str);
				buf.mtype = 1;
				pthread_mutex_lock(&shared_mutex);
				msgctl (client_data.msqid, IPC_STAT, &qstatus);
				//printf("%ld\n",qstatus.msg_qnum);
				if (qstatus.msg_qnum < N)
				{
					msgsnd(client_data.msqid, &buf, sizeof(buf), 0);
				}
				else
				{
					printf("Queue is full\n");
				}	
				pthread_mutex_unlock(&shared_mutex);
				break;
			case 2:
				pthread_mutex_lock(&shared_mutex);
				
				msgctl (client_data.msqid, IPC_STAT, &qstatus);
				if (qstatus.msg_qnum > 0)
				{
					msgrcv(client_data.msqid, &buf, sizeof(buf), 1, 0);
				}
				else
				{
					printf("Queue is empty\n");
				}
				if ((send(client_data.connSock, &buf, sizeof(buf), 0)) <= 0)
				{
					perror("\033[31mSERVER\033[0m: send() failed");
					sleep(5);
					pthread_exit(0);	
				}
				pthread_mutex_unlock(&shared_mutex);
				sleep(buf.T);
				break;
		}
	}
}

void* thread_TCP(void* arg_TCP)
{
	pthread_detach(pthread_self());
	client_struct temp = *(client_struct*)arg_TCP;
	int type = temp.type;
	
	int tcp_port;
	switch (type)
	{
		case 1:
			tcp_port = TCP_PORT1;
			break;
		case 2:
			tcp_port = TCP_PORT2;
			break;
	}
	int TCP_socket;
	struct sockaddr_in TCP_addr;
	TCP_addr.sin_family = AF_INET;
	TCP_addr.sin_port = htons(tcp_port);
	TCP_addr.sin_addr.s_addr = htonl(INADDR_ANY);

	TCP_socket = socket(AF_INET, SOCK_STREAM, 0);
	bind(TCP_socket, (struct sockaddr*) &TCP_addr, sizeof(TCP_addr));
	listen(TCP_socket, 5);
	int connSock;

	while(1)
	{
		connSock = accept(TCP_socket, NULL, NULL);
		temp.connSock = connSock;
		pthread_t client_tid;
		pthread_create(&client_tid, 0, client_thread, &temp);
	}

}

int main(int argc, char const *argv[])
{
	struct tcp_msg buf;
	buf.mtype = 1;
	int msqid;

	client_struct temp_main1, temp_main2;

	pthread_t udp_tid;

	msqid = msgget(IPC_PRIVATE, 0600 | IPC_CREAT);
	temp_main1.msqid = msqid; 
	temp_main2.msqid = msqid; 
	pthread_create(&udp_tid, 0, thread_UDP, &msqid);

	pthread_t tcp_tid1, tcp_tid2;

	temp_main1.type = 1;
	pthread_create(&tcp_tid1, 0, thread_TCP ,&temp_main1);
	temp_main2.type = 2;
	pthread_create(&tcp_tid2, 0, thread_TCP ,&temp_main2);
	getchar();
 //	recv(,,sizeof(msg),0)
	return 0;
}
