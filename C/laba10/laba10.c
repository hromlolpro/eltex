#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <time.h>

 struct data{
	int* targets;
	int** map;
	int sizemap;
	int n;

};

typedef struct data data;

static pthread_mutex_t shared_mutex  = PTHREAD_MUTEX_INITIALIZER;

int** mapcreate (int sizemap)
{	
	srand(getpid());
	int **map;
	map = malloc(sizemap * sizeof(int*));
	for (int i = 0; i < sizemap; ++i)
	{
		map[i] = malloc(sizemap * sizeof(int));
		for (int j = 0; j < sizemap; ++j)
		{
			map[i][j] = rand()%2;
			printf("%d  ", map[i][j]);
		}
		printf("\n");
	}
	return map;
}

int solution (int a, int b, int sizemap)
{
	int rest_a = sizemap - a; //Расстояние от правого края
	int rest_b = sizemap - b; //Расстояние от нижнего края
	int var;
	if (rest_a >= a && rest_b > b)
	{
	 	var = 0;
	}
	else if (rest_a < a && rest_b >= b)
	{
		var = 2;
	}
	else if (rest_a > a && rest_b <= b)
	{
		var = 1;
	}
	else if (rest_a <= a && rest_b < b)
	{
		var = 3;
	}
	else
	{
		var = rand()%4;
	}
	return(var);
}

void* drone(void* arg)
{
	//pthread_mutex_lock(&shared_mutex);
	data* data_drones = (data*) arg;
	int *targets = data_drones->targets;
	int sizemap = data_drones->sizemap;
	int **map = data_drones->map;
	int n;

	pthread_mutex_lock(&shared_mutex);
	n = data_drones->n;
	data_drones->n++;
	pthread_mutex_unlock(&shared_mutex);

			srand(pthread_self());
			int a = rand()%sizemap; //Начальная точка дрона в строке
			int b = rand()%sizemap; //Начальная точка дрона в столбце
			printf("Разведчик №%d [%lu] упал в a=%d, b=%d\n", n, pthread_self(), a, b);
			int var = solution(a, b, sizemap);		
			switch (var){
			case 0:
				switch (rand()%2){
					case 0: //Проходка от а до sizemap
						for (int i = b; i < sizemap; ++i)
						{
							if (map[a][i] == 1)
								targets[n]++;
						}
						
						break;
					case 1: //Проходка от b до sizemap
						for (int i = a; i < sizemap; ++i)
						{
							if (map[i][b] == 1)
								targets[n]++;
						}
						
						break;
				}
				break;
			case 1:
				switch (rand()%2){
					case 0: 
						for (int i = b; i >= 0; i--)
						{
							if (map[a][i] == 1)
								targets[n]++;
						}
						
						break;
					case 1:
						for (int i = a; i < sizemap; ++i)
						{
							if (map[i][b] == 1)
								targets[n]++;
						}
						

						break;
				}
				break;
			case 2:
				switch (rand()%2){
					case 0: 
						for (int i = b; i < sizemap; ++i)
						{
							if (map[a][i] == 1)
								targets[n]++;
						}
						
						break;
					case 1:
						for (int i = a; i >= 0; i--)
						{
							if (map[i][b] == 1)
								targets[n]++;
						}
						
						break;
				}
				break;
			case 3:
				switch (rand()%2){
					case 0: 
						for (int i = b; i >= 0; i--)
						{
							if (map[a][i] == 1)
								targets[n]++;
						}
						
						break;
					case 1:
						for (int i = a; i >= 0; i--)
						{
							if (map[i][b] == 1)
								targets[n]++;
						}
						
						break;
				}
				break;			
			}


	//pthread_mutex_unlock(&shared_mutex);

}

int main(int argc, char const *argv[])
{
	data data_main;
	int drones = atoi(argv[2]);
	int sizemap = atoi(argv[1]);
	data_main.sizemap=sizemap;
	data_main.targets=malloc(sizeof(int)*drones);
	int **map;
	pthread_t tid[drones];
	map = mapcreate(sizemap);
	data_main.map=map;
	for (int i = 0; i < drones; ++i)
	{	
		pthread_create(&tid[i], NULL, drone, &data_main);
		
	}
	for (int i = 0; i < drones; ++i)
	{
		pthread_join(tid[i], NULL);
		printf("Разведчик [%d] обнаружил целей = %d\n", i, data_main.targets[i]);
		
	}
	for (int i = 0; i < sizemap; ++i)
	{
		free(map[i]);
	}
	free(map);
	free(data_main.targets);
	
	return 0;
}