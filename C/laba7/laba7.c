#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

//argv[1] - размерность матрицы (квадратной)
//argv[2] - количество разведчиков



int main(int argc, char const *argv[])
{
	int counttarget = 0;
	int sizemap = atoi(argv[1]);
	int drones = atoi(argv[2]);
	pid_t pid[drones];
	int fd[drones][2];
	int map[sizemap][sizemap];
	int targets[drones];
	srand(getpid());
	//Начало заполнение матрицы
	for (int i = 0; i < sizemap; ++i)
	{
		for (int j = 0; j < sizemap; ++j)
		{
			map[i][j] = rand()%2;
			printf("%d  ", map[i][j]);
			if (map[i][j] == 1)
			{
				counttarget++;
			}
		}
		printf("\n");
	}
	// Конец заполнения матрицы и ее вывод
	printf("%d\n", counttarget);
	for (int n = 0; n < drones; ++n)
	{
		pipe(fd[n]);
		pid[n] = fork();

		//Начало работы дронов
		if (pid[n] == 0)
		{
			close(fd[n][0]);
			int foundtarget = 0;
			srand(getpid());
			int a = rand()%sizemap; //Начальная точка дрона в строке
			int b = rand()%sizemap; //Начальная точка дрона в столбце
			printf("Разведчик %d упал в a=%d, b=%d\n", n, a, b);
			int rest_a = sizemap - a; //Расстояние от правого края
			int rest_b = sizemap - b; //Расстояние от нижнего края

			int var;
			if (rest_a >= a && rest_b > b)
			{
			 	var = 0;
			}
			else if (rest_a < a && rest_b >= b)
			{
				var = 2;
			}
			else if (rest_a > a && rest_b <= b)
			{
				var = 1;
			}
			else if (rest_a <= a && rest_b < b)
			{
				var = 3;
			}
			else
			{
				printf("%s\n", "V seredinu padat' ploho, fu, plohoi razvedchik, FU!");
				for (int i = a; i < sizemap; ++i)
						{
							if (map[a][i] == 1)
								foundtarget++;
						}
						write(fd[n][1], &foundtarget, sizeof(int));
						exit(0);
			}
			switch (var){
			case 0:
				switch (rand()%2){
					case 0: //Проходка от а до sizemap
						for (int i = b; i < sizemap; ++i)
						{
							if (map[a][i] == 1)
								foundtarget++;
						}
						write(fd[n][1], &foundtarget, sizeof(int));
						exit(0);
						break;
					case 1: //Проходка от b до sizemap
						for (int i = a; i < sizemap; ++i)
						{
							if (map[i][b] == 1)
								foundtarget++;
						}
						write(fd[n][1], &foundtarget, sizeof(int));
						exit(0);
						break;
				}
				break;
			case 1:
				switch (rand()%2){
					case 0: 
						for (int i = b; i >= 0; i--)
						{
							if (map[a][i] == 1)
								foundtarget++;
						}
						write(fd[n][1], &foundtarget, sizeof(int));
						exit(0);
						break;
					case 1:
						for (int i = a; i < sizemap; ++i)
						{
							if (map[i][b] == 1)
								foundtarget++;
						}
						write(fd[n][1], &foundtarget, sizeof(int));
						exit(0);

						break;
				}
				break;
			case 2:
				switch (rand()%2){
					case 0: 
						for (int i = b; i < sizemap; ++i)
						{
							if (map[a][i] == 1)
								foundtarget++;
						}
						write(fd[n][1], &foundtarget, sizeof(int));
						exit(0);
						break;
					case 1:
						for (int i = a; i >= 0; i--)
						{
							if (map[i][b] == 1)
								foundtarget++;
						}
						write(fd[n][1], &foundtarget, sizeof(int));
						exit(0);
						break;
				}
				break;
			case 3:
				switch (rand()%2){
					case 0: 
						for (int i = b; i >= 0; i--)
						{
							if (map[a][i] == 1)
								foundtarget++;
						}
						write(fd[n][1], &foundtarget, sizeof(int));
						exit(0);
						break;
					case 1:
						for (int i = a; i >= 0; i--)
						{
							if (map[i][b] == 1)
								foundtarget++;
						}
						write(fd[n][1], &foundtarget, sizeof(int));
						exit(0);
						break;
				}
				break;			
			}

		}
		//Дроны закончили свою работу
	}
	for (int i = 0; i < drones; ++i)
	{
		waitpid(pid[i], NULL, 0);
		close(fd[i][1]);
		read(fd[i][0], &(targets[i]), sizeof(int));
		printf("Разведчик %d: [%d] обнаружил целей = %d\n", i, pid[i], targets[i]);
	}
	return 0;
}