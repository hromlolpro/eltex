#include <stdio.h>
#include <stdlib.h>
extern float degree(float a);
extern float root(float b);

int main(int argc, char const *argv[])
{
	printf("Square degree = %f\n", degree(atof(argv[1])));
	printf("Square root = %f\n", root(atof(argv[1])));
	return 0;
}