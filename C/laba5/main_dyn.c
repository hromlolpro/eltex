#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>

int main(int argc, char const *argv[])
{
	void* handler;
	handler=dlopen("/home/hromlolpro/agafon/eltex/C/laba5/libsquaredyn.so", RTLD_LAZY);
	float (*fdegree)(float);
	float (*froot)(float);
	if (!handler)
	{
		fprintf(stderr, "dlopen() error: %s\n", dlerror());
		return 1;
	}
	fdegree=dlsym(handler, "degree");
	froot=dlsym(handler,"root");
	printf("Square degree = %f\n",(*fdegree)(atof(argv[1])));
	printf("Square root = %f\n",(*froot)(atof(argv[1])));
	return 0;
}