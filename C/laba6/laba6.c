#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/file.h>
#include <signal.h>

int main(int argc, char const *argv[])
{
	pid_t p[atoi(argv[1])];
	FILE* fp;
	FILE* pf;
	int honey=0;
	fp=fopen("honey.txt", "w+");
	if(!fp)
        perror("fopen");
	rewind(fp);
	fprintf(fp, "%d", 0);
	fclose(fp);

	for (int i = 0; i < atoi(argv[1]); ++i)
	{
		p[i] = fork();
		if (p[i] == 0)
		{

			if (i == 0)
			{
				for(;;)
				{
					sleep(3);
					pf=fopen("honey.txt", "r+");
					while(flock(fileno(pf), LOCK_EX)) sleep(0.5);
					fscanf(pf, "%d", &honey);
					rewind(pf);
					if (honey < atoi(argv[3]))
					{
						fprintf(pf, "%d\n", 0);
						flock(fileno (pf), LOCK_UN);
						printf("Медведь съел меда = %d, медведь пошел голодный спать\n", honey);
						fclose(pf);
						sleep(3);
						pf=fopen("honey.txt", "r+");
						while(flock(fileno(pf), LOCK_EX)) sleep(0.5);
						fscanf(pf,"%d", &honey);
						rewind(pf);
						if (honey < atoi(argv[3]))
						{
							printf("Медведь умер\n");
							fclose(pf);
							return 0;
						}


					}
					fprintf(pf, "%d\n", honey-(atoi(argv[3])));
					rewind(pf);
					fscanf(pf, "%d", &honey);
					printf("Медведь поел меда, осталось меда = %d\n", honey);
					flock(fileno (pf), LOCK_UN);
					fclose(pf);
				}
			}
		
			printf("Пчела %d+%d начала работу\n", i, getpid() );
			for(;;)
			{
				sleep(rand()%3+1);
				pf=fopen("honey.txt", "r+");
				while(flock(fileno(pf), LOCK_EX)) sleep(0.5)
					fscanf(pf, "%d", &honey);
					rewind(pf);
					fprintf(pf, "%d\n", honey+(atoi(argv[2])));
					rewind(pf);
					fscanf(pf, "%d", &honey);
					printf("Пчела PID=%d принесла мед, Количество меда всего = %d\n", getpid(), honey);
					flock(fileno (pf), LOCK_UN);
					fclose(pf);
			}		
		}
	}
	waitpid(p[0], NULL, 0);
	for (int i = 1; i < atoi(argv[1]); ++i)
	{
		kill(p[i], SIGTERM);
		printf("Пчела убита PID=%d\n", p[i]);
	}

	return 0;
}