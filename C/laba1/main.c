#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define max_len 100
char** input(int* count);
int funccmp (const void* str1, const void* str2);

int main(int argc, char **argv)
{
	char **str;
	int count;
	str=input(&count);
	qsort(str, count, sizeof(char*), funccmp);
	for (int i = 0; i < count; i++)
	{
		printf("[%d]%s\n", i , str[i]);
		free(str[i]);
	}
	free(str);

	return 0;
}

char** input(int* count){
	char** fstr;
	printf("Введите количество строк\n");
	scanf("%d", count);
	fstr=malloc((*count)*sizeof(*fstr));
	printf("Введите %d строк\n", (*count));
	for (int i = 0; i <(*count); i++)
	{
		fstr[i]=malloc(max_len*sizeof(**fstr));
		scanf("%s", fstr[i]);
	}
return fstr; 
}

int funccmp (const void* str1, const void* str2) {
	return (strlen(*(char**)str2) - strlen(*(char**)str1));
}
