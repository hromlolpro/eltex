#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/msg.h>

struct msg
{
	long mtype;
	pid_t pid;
	int data;

};

int** mapcreate (int sizemap)
{	
	srand(getpid());
	int **map;
	map = malloc(sizemap * sizeof(int*));
	for (int i = 0; i < sizemap; ++i)
	{
		map[i] = malloc(sizemap * sizeof(int));
		for (int j = 0; j < sizemap; ++j)
		{
			map[i][j] = rand()%2;
			printf("%d  ", map[i][j]);
		}
		printf("\n");
	}
	return map;
}

int solution (int a, int b, int sizemap)
{
	int rest_a = sizemap - a; //Расстояние от правого края
	int rest_b = sizemap - b; //Расстояние от нижнего края
	int var;
	if (rest_a >= a && rest_b > b)
	{
	 	var = 0;
	}
	else if (rest_a < a && rest_b >= b)
	{
		var = 2;
	}
	else if (rest_a > a && rest_b <= b)
	{
		var = 1;
	}
	else if (rest_a <= a && rest_b < b)
	{
		var = 3;
	}
	else
	{
		var = rand()%4;
	}
	return(var);
}

int main(int argc, char const *argv[])
{

	struct msg buf;
	buf.mtype = 1;
	int msqid;
	msqid = msgget(IPC_PRIVATE, 0600 | IPC_CREAT);
	int drones = atoi(argv[2]);
	int sizemap = atoi(argv[1]);
	pid_t pid[drones];
	int foundtargets[drones];
	int **map;
	map = mapcreate(sizemap);
	for (int i = 0; i < drones; ++i)
	{
		pid[i]=fork();

		//Начало работы дронов
		if (pid[i] == 0)
		{
			buf.data = 0;
			buf.pid = getpid();
			srand(getpid());
			int a = rand()%sizemap; //Начальная точка дрона в строке
			int b = rand()%sizemap; //Начальная точка дрона в столбце
			printf("Разведчик %d упал в a=%d, b=%d\n", i, a, b);
			int var = solution(a, b, sizemap);		
			switch (var){
			case 0:
				switch (rand()%2){
					case 0: //Проходка от а до sizemap
						for (int i = b; i < sizemap; ++i)
						{
							if (map[a][i] == 1)
								buf.data++;
						}
						msgsnd(msqid, &buf, sizeof(buf), 0);
						exit(0);
						break;
					case 1: //Проходка от b до sizemap
						for (int i = a; i < sizemap; ++i)
						{
							if (map[i][b] == 1)
								buf.data++;
						}
						msgsnd(msqid, &buf, sizeof(buf), 0);
						exit(0);
						break;
				}
				break;
			case 1:
				switch (rand()%2){
					case 0: 
						for (int i = b; i >= 0; i--)
						{
							if (map[a][i] == 1)
								buf.data++;
						}
						msgsnd(msqid, &buf, sizeof(buf), 0);
						exit(0);
						break;
					case 1:
						for (int i = a; i < sizemap; ++i)
						{
							if (map[i][b] == 1)
								buf.data++;
						}
						msgsnd(msqid, &buf, sizeof(buf), 0);
						exit(0);

						break;
				}
				break;
			case 2:
				switch (rand()%2){
					case 0: 
						for (int i = b; i < sizemap; ++i)
						{
							if (map[a][i] == 1)
								buf.data++;
						}
						msgsnd(msqid, &buf, sizeof(buf), 0);
						exit(0);
						break;
					case 1:
						for (int i = a; i >= 0; i--)
						{
							if (map[i][b] == 1)
								buf.data++;
						}
						msgsnd(msqid, &buf, sizeof(buf), 0);
						exit(0);
						break;
				}
				break;
			case 3:
				switch (rand()%2){
					case 0: 
						for (int i = b; i >= 0; i--)
						{
							if (map[a][i] == 1)
								buf.data++;
						}
						msgsnd(msqid, &buf, sizeof(buf), 0);
						exit(0);
						break;
					case 1:
						for (int i = a; i >= 0; i--)
						{
							if (map[i][b] == 1)
								buf.data++;
						}
						msgsnd(msqid, &buf, sizeof(buf), 0);
						exit(0);
						break;
				}
				break;			
			}

		}
	}

	for (int i = 0; i < drones; ++i)
	{
		waitpid(pid[i], NULL, 0);
	}
		//Дроны закончили свою работу
	for (int i = 0; i < drones; ++i)
	{
		msgrcv(msqid, &buf, sizeof(buf), buf.mtype, 0);
		foundtargets[i] = buf.data;
		pid[i] = buf.pid;
		printf("Разведчик %d: [%d] обнаружил целей = %d\n", i, pid[i], foundtargets[i]);
	}
	return 0;
}
