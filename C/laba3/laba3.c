#include <stdio.h>
#include <stdlib.h>
#include <string.h>
struct book
{
	char name[100];
	int year;
	int amount;
	int price;
};

typedef struct book bs;

void input (bs** book, int count)
{
	for (int i = 0; i < count; ++i)	
	{
	book[i]=malloc(sizeof(bs));
	printf("Введите название книги\n");
	scanf("%s", book[i]->name);
	printf("Введите год издания\n");
	scanf("%d", &book[i]->year);
	printf("Количество страниц\n");
	scanf("%d", &book[i]->amount);
	printf("Стоимость книги\n");
	scanf("%d", &book[i]->price);	
	}
}
void output (bs** book, int count){
for (int i = 0; i < count; ++i)
{
	printf("Название книги: %s\n",book[i]->name);
	printf("Год издания: %d\n",book[i]->year);
	printf("Количесто страниц: %d\n",book[i]->amount);
	printf("Стоимость книги: %d\n",book[i]->price);
}
}

int cmp(const void *p1, const void *p2)
{
    bs * bs1 = *(bs**)p1;
    bs * bs2 = *(bs**)p2;
    return strcmp(bs2->name, bs1->name);
}


/*int cmp (const void* p1, const void* p2) {
	return (((*(bs**)p1)->name)[0] - ((*(bs**)p2)->name)[0]);
}
*/

int main(int argc, char const *argv[])
{
	int count;
	printf("Введите количество книг\n");
	scanf("%d", &count);
	bs** book=malloc(count*sizeof(bs**));
	input(book, count);
	qsort(book, count, sizeof(bs*), cmp);
	output(book, count);

	for (int i = 0; i < count; i++)
    {
        free(book[i]);
    }
    free(book);
    return 0;
}
